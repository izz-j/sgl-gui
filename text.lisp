(in-package #:sgl-gui)


(defstruct text-format ()
 space-between
  line-number
  spacing
  margin
  return
  new-line)

(defparameter *first-letter* t)

(defun load-key-textures (chars path)
  (loop for item-pair in chars
        collect (sgl:load-texture (concatenate 'string path (cadr item-pair)))))

(defun bind-key-textures (sprite chars texture-list)
  (loop for item-pair in chars
        for count = 0 then (+ count 1)
        if (equal sprite (cadr item-pair))
        do (gl:bind-texture :texture-2d (nth count texture-list))
        and do (return)))
  
(defun enter-key-text (format)
  (setf (text-format-return format) (text-format-new-line format))
  (setf (text-format-spacing format) (text-format-margin format)))

(defun get-char-path (c chars)
  "return char path from list"
  (loop for item-pair in chars
        if (equal c (car item-pair))
        return (cdr item-pair)))
(defparameter *between* 0)
(defparameter *prev* nil)

(defun draw-text (path width height format c texture-list)
  (let* ((sprite nil))
    (if (eq *first-letter* t)
        (progn
          (setf (text-format-spacing format) (+ (text-format-spacing format) (text-format-margin format)))
          (setf *first-letter* nil)))
    (if (equal c #\Space)
        (progn
          (setf (text-format-spacing format) (+ (text-format-spacing format) 1))
          (return-from draw-text)))
    (if (equal c #\|)
        ;;needs work. should be a return key
        (progn
          (enter-key-text format)
          (return-from draw-text)))
    (setf sprite (car (get-char-path c *chars*)))
    ;;bind texture for char here
    (bind-key-textures sprite *chars* texture-list)
    (setf path (concatenate 'string path sprite))
    (setf (text-format-spacing format) (+ (text-format-spacing format) (text-format-space-between format)))
    (let ((y (- (text-format-return format) height)))
      (if (equal sprite "l.png")
          (setf (text-format-spacing format) (- (text-format-spacing format) 2)))
      
      (if (or (equal sprite "j.png") (equal sprite "i.png") (equal sprite "W.png"))
          (setf (text-format-spacing format) (- (text-format-spacing format) 5)))
      
      (if (or (and (equal *prev* "t.png") (equal sprite "t.png")) (and (equal *prev* "l.png") (equal sprite "f.png")) (and (equal *prev* "J.png") (equal sprite "o.png")) (and (equal *prev* "'.png") (equal sprite "s.png")) (and (equal *prev* "'.png") (equal sprite "t.png")) (and (equal *prev* "a.png") (equal sprite "t.png")) (and (equal *prev* "f.png") (equal sprite "f.png")) (and (equal *prev* "J.png") (equal sprite "a.png")) (and (equal *prev* "I.png") (or (equal sprite "s.png") (equal sprite "t.png"))) (and (equal *prev* "l.png") (or (equal sprite "l.png") (equal sprite "i.png"))))
          (setf (text-format-spacing format) (- (text-format-spacing format) 3)))
      
      (if (and (equal *prev* "A.png") (equal sprite "l.png"))
          (setf (text-format-spacing format) (+ (text-format-spacing format) 3)))
      (if (and (equal *prev* "w.png") (or (equal sprite "e.png") (equal sprite "a.png")))
          (setf (text-format-spacing format) (+ (text-format-spacing format) 3)))
      (if (and (equal *prev* "D.png") (equal sprite ").png"))
          (setf (text-format-spacing format) (+ (text-format-spacing format) 3)))

      (if (or (and (equal *prev* "W.png") (equal sprite "e.png")) (and (equal *prev* "D.png") (equal sprite "r.png")) (and (equal *prev* "D.png") (equal sprite "a.png")) (and (equal *prev* "B.png") (equal sprite "e.png")) (and (equal *prev* "n.png") (equal sprite "j.png")) (and (equal *prev* "k.png") (equal sprite "n.png")) (and (equal *prev* "u.png") (equal sprite "l.png")) (and (equal *prev* "n.png") (equal sprite "g.png")) (and (equal *prev* "N.png") (equal sprite "o.png"))  (and (equal *prev* "u.png") (equal sprite "m.png")) (and (equal *prev* "U.png") (equal sprite "n.png")) (and (equal *prev* "o.png") (equal sprite "m.png")) (and (equal *prev* "m.png") (equal sprite "e.png")) (and (equal *prev* "w.png") (equal sprite "s.png")) (and (equal *prev* "T.png") (equal sprite "h.png")) (and (equal *prev* "W.png") (equal sprite "h.png")) (and (equal *prev* "M.png") (or (equal sprite "y.png") (equal sprite "a.png"))) (and (equal *prev* "D.png") (or (equal sprite "o.png") (equal sprite "i.png"))) (and (equal *prev* "B.png") (equal sprite "u.png")))
          (setf (text-format-spacing format) (+ (text-format-spacing format) 3)))

      (if (and (equal *prev* "w.png") (equal sprite "h.png"))
          (setf (text-format-spacing format) (+ (text-format-spacing format) 3)))
               
      (if (equal sprite "w.png")
          (setf (text-format-spacing format) (+ (text-format-spacing format) 3)))
      
      (if (and (equal *prev* "H.png") (equal sprite "e.png"))
          (setf (text-format-spacing format) (+ (text-format-spacing format) 5)))
      
      (if (equal sprite "m.png")
          (setf (text-format-spacing format) (+ (text-format-spacing format) 5)))
   ;  (if (or (equal sprite "p.png") (equal sprite "g.png") (equal sprite "y.png") (equal sprite "q.png") (equal sprite "j.png"))
                                        ;      (setf y (+ y 10)))
      (setf *prev* sprite)
      (sgl:display-sprite path width height 1 1 0 0 :xshift (text-format-spacing format) :yshift y))))

(defun text (path width height format s texture-list)
  (loop for c across s
     ;;do (princ c)
        do (draw-text path width height format c texture-list)
        do (setf (text-format-spacing format) (+ 10 (text-format-spacing format)))))

(defun write-text (path format s texture-list &key (width 80) (height 80))
  (let ((first-line (text-format-return format)))
    (text path width height format s texture-list)
    ;(setf (text-format-spacing format) (text-format-margin format))
    (setf (text-format-spacing format) 10)
    (setf (text-format-return format) first-line)))
