;;;; mgui.lisp
(in-package #:sgl-gui)
#|(defun find-matchp (c list)
  (loop l across list
        if (equal c l)
        do (return t))
(return nil))|#
(defstruct choice-module
  highlighter
  positions
  current-position
  choices
  outcome
  outcomes
  (update-highlighter (lambda (position-index positions)
                        (nth position-index positions)))
  (draw-highlighter (lambda (highlighter current-position)
                      (sgl:load-texture highlighter)
                      (sgl:display-sprite highlighter 1 1 0 0 :yshift current-position)))
  (update-outcome (lambda (outcome-index outcomes)
                    (nth outcome-index outcomes))))

(defun quick-draw-highlighter (choice)
  (funcall (choice-module-draw-highlighter choice)
           (choice-module-highlighter choice) (choice-module-current-position choice))) 

(defun make-panel (box-path border-path y)
  (list box-path border-path y))

(defun valid-new-width-p (width new-width)
  (if (not (integerp width))
      (return-from valid-new-width-p nil))
  (if (equal width new-width)
      (return-from valid-new-width-p t))
  (valid-new-width-p (setf width (/ width 2)) new-width))
         
        ;;Include this function below in sgl library when finished
(defun scale-down-texture (path new-width new-height &key (x0 0) (y0 0))
  "Sloppy and dirty way to load and scale texture.
   x0 and y0 shift initial positions. Decrease width by dividing 2 into
   the original width, to shrink further divide 2 again and so on.."
  ;;catch an error
  (if (not (valid-new-width-p (sgl:get-image-width path) new-width))
      (progn
        (princ "new width is not a factor of original width of texture and 2")
        (terpri)))
  (gl:bind-texture :texture-2d (gl:gen-texture))
  (gl:tex-parameter  :texture-2d :texture-min-filter :nearest)
;; x direction
  (gl:tex-parameter  :texture-2d :texture-wrap-s :clamp-to-edge)
  ;;y direction
  (gl:tex-parameter  :texture-2d :texture-wrap-t :clamp-to-edge)
  ;;Empty texture
  (gl:tex-image-2d :texture-2d 0 :RGBA8 (sgl:get-image-width path) (sgl:get-image-height path) 0 :rgba :unsigned-byte nil)
  ;;Sub texture within empty texture and scale
  (gl:tex-sub-image-2d :texture-2d 0  x0 y0 new-width new-height :rgba :unsigned-byte (sdl2:surface-pixels (sdl2-image:load-image path)))
  (gl:enable :texture-2d))

#|(defun scale-down-texture2 (path new-width new-height)
  "scale textures with smoos"
;;  (gl:generate-mipmap :texture-2d)
  (gl:bind-texture :texture-2d (gl:gen-texture))
;;  (gl:generate-mipmap :texture-2d)
  (gl:tex-parameter :texture-2d :texture-min-filter :nearest)
  (gl:tex-parameter :texture-2d :texture-wrap-s :repeat)
  (gl:tex-parameter :texture-2d :texture-wrap-t :repeat)
  (gl:tex-image-2d :texture-2d 0 :RGBA8 1000 1000 0 :rgba :unsigned-byte  nil)
  (gl:tex-image-2d :texture-2d 0 :RGBA8 (/ new-width 2) (/ new-height 2) 0 :rgba :unsigned-byte (sdl2:surface-pixels (sdl2-image:load-image path)))
  (gl:enable :texture-2d))|#
  
(defun draw-ui-panel (box-path &key (border-path nil) (y 0))
;  (sgl:load-texture box-path)
  (sgl:display-sprite box-path 1 1 0 0 :yshift y)
  (if (not (equal border-path nil))
      (progn
      (sgl:load-texture border-path)
      (sgl:display-sprite border-path 1 1 0 0 :yshift y))))
