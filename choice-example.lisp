(in-package #:sgl-gui)


(defparameter *dialogue-panel* (sgl-gui:make-panel "choice-panel.png" nil 680))

(defparameter *max-lines* 1)

(defparameter *dialogue* "I think we should go back to the ship.")


(defparameter *tf* (sgl-gui:make-text-format :line-number 0 :spacing 0 :margin 30 :return 690 :new-line 720))
(defparameter *tf2* (sgl-gui:make-text-format :line-number 0 :spacing 0 :margin 30 :return 720))

(defparameter *dialogue-mode* t)
 
(defparameter *choice* (make-choice-module :highlighter "highlighter.png"
                                              :positions (list 688 718) :current-position 688
                                              :choices (list "Yeah. Let's get out of here." "No. I think we can still get more information.")
                                              :outcome nil
                                              :outcomes (list "Come on then!" "Pfft! Fine. But I think we are wasting our time.")))

(glfw:def-key-callback highlight (window key scancode action mod-keys)
  (declare (ignore window scancode mod-keys))
  (when (and (< (text-format-line-number *tf*) *max-lines*) (or (and (eq key :up) (eq action :press)) (and (eq key :up) (eq action :repeat))))
    (setf (choice-module-current-position *choice*) (funcall (choice-module-update-highlighter *choice*) 0 (choice-module-positions *choice*))))
  (when (and (< (text-format-line-number *tf*) *max-lines*) (or (and (eq key :down) (eq action :press)) (and (eq key :down) (eq action :repeat))))
    (setf (choice-module-current-position *choice*) (funcall (choice-module-update-highlighter *choice*) 1 (choice-module-positions *choice*))))
  (when (and (< (text-format-line-number *tf*) *max-lines*) (and (and (eq key :enter) (eq action :press)) (eq (choice-module-current-position *choice*) (car (choice-module-positions *choice*)))))
    (setf (choice-module-outcome *choice*) (funcall (choice-module-update-outcome *choice*) 0 (choice-module-outcomes *choice*))))
  (when (and (< (text-format-line-number *tf*) *max-lines*) (and (and (eq key :enter) (eq action :press)) (eq (choice-module-current-position *choice*) (cadr (choice-module-positions *choice*)))))
    (setf (choice-module-outcome *choice*) (funcall (choice-module-update-outcome *choice*) 1 (choice-module-outcomes *choice*)))))

(glfw:def-key-callback next-text (window key scancode action mod-keys)
  (declare (ignore window scancode mod-keys))
  (when (and (< (text-format-line-number *tf*) *max-lines*) (or (and (eq key :a) (eq action :press)) (and (eq key :a) (eq action :repeat))))
    (setf (text-format-line-number *tf*) (+ (text-format-line-number *tf*) 1))))

(glfw:def-key-callback next (window key scancode action mod-keys)
  (declare (ignore window scancode mod-keys))
  (when (and (eq key :enter) (eq action :press))
      (setf *dialogue-mode* nil)))

(glfw:def-key-callback quit-on-escape (window key scancode action mod-keys)
  (declare (ignore window scancode mod-keys))
  (when (and (eq key :escape) (eq action :press))
    (glfw:set-window-should-close)))

(glfw:def-window-size-callback update-viewport (window w h)
  ;;(princ (car (glfw:get-window-size window)))
  ;;(terpri)
  (if (< (car (glfw:get-window-size window)) 1000)
      (sgl:set-viewport 0 0 w h)
      (sgl:set-viewport 500 0  w h)))

(defun background ()
  (sgl:load-texture "bar.png")
  (sgl:display-sprite "bar.png" 1 1 0 0))


(defun draw-scene (font-path format format2)
  (background)
  ;;draw character
  (sgl:load-texture "cd happy.png")
  (sgl:display-sprite "cd happy.png" 1 1 0 0 :yshift 200 :xshift 50)
  ;;draw panels
 ; (glfw:set-key-callback 'next)
  (sgl-gui:draw-ui-panel (car *dialogue-panel*) :y (caddr *dialogue-panel*))
  (glfw:set-key-callback 'next)
  (if (eq *dialogue-mode* t)
      (sgl-gui:write-text font-path format *dialogue*))
  (if (and (eq (choice-module-outcome *choice*) nil) (eq *dialogue-mode* nil))
      (progn
        (glfw:set-key-callback 'highlight)
        (quick-draw-highlighter *choice*)
        (sgl-gui:write-text font-path format (nth 0 (choice-module-choices *choice*)))
        (sgl-gui:write-text font-path format2 (nth 1 (choice-module-choices *choice*)))))
  (if (not (eq (choice-module-outcome *choice*) nil))
      (sgl-gui:write-text font-path format (choice-module-outcome *choice*))))
  
(defun choice-example ()
  (sdl2-image:init '(:png))
  (glfw:with-init-window (:title "Test Window" :width 800 :height 1000)
    (let ((font-path "noto-vegas/")) 
      (glfw:set-window-size-callback 'update-viewport)
      (glfw:set-key-callback 'quit-on-escape)
      ;(glfw:set-key-callback 'next-text)
      ;; .3 .3. 3 for gray
      (gl:clear-color 0 0 1 1)
      (loop until (glfw:window-should-close-p)
            do (gl:clear :color-buffer-bit)
            do (draw-scene font-path *tf* *tf2*)
            do (gl:clear)
	    do (glfw:poll-events)
	    do (glfw:swap-buffers))
      (setf (text-format-line-number *tf*) 0)
      (setf *dialogue-mode* t)
      (setf (choice-module-outcome *choice*) nil))))

