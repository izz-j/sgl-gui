;;;; mgui.asd

(asdf:defsystem #:sgl-gui
  :description "An extension to SGL that provides functions to quickly make ui"
  :author "Iseman Johnson"
  :license  "MIT"
  :version "0.0.1"
  :serial t
  :depends-on (#:sgl)
  :components ((:file "package")
               (:file "keys")
               (:file "text")
               (:file "sgl-gui")
               (:file "dialogue-example")
               (:file "choice-example")))
