(in-package #:sgl-gui)

(defparameter *dialogue-panel* (sgl-gui:make-panel "box2.png" nil 680))

(defparameter *max-lines* 2)

(defparameter *dialogue-list* (list "Hey! It's so good to see you." "I was so worried about you! I heard the news about|what your brother did." "I see.."))

(defparameter *tf* (sgl-gui:make-text-format :line-number 0 :spacing 0 :margin 30 :return 690 :new-line 720))

(glfw:def-key-callback next-text (window key scancode action mod-keys)
  (declare (ignore window scancode mod-keys))
  (when (and (< (text-format-line-number *tf*) *max-lines*) (or (and (eq key :enter) (eq action :press)) (and (eq key :enter) (eq action :repeat))))
    (setf (text-format-line-number *tf*) (+ (text-format-line-number *tf*) 1))))

(glfw:def-key-callback quit-on-escape (window key scancode action mod-keys)
  (declare (ignore window scancode mod-keys))
  (when (and (eq key :escape) (eq action :press))
    (glfw:set-window-should-close)))

(glfw:def-window-size-callback update-viewport (window w h)
  ;;(princ (car (glfw:get-window-size window)))
  ;;(terpri)
  (if (< (car (glfw:get-window-size window)) 1000)
      (sgl:set-viewport 0 0 w h)
      (sgl:set-viewport 500 0  w h)))

(defun background ()
(gl:pixel-store :unpack-alignment 1)
(sgl:load-texture "bar.png")
(sgl:display-sprite "bar.png" 1 1 0 0))

(defun draw-scene-1 (font-path format)
  ;;draw character
  (sgl:load-texture "cd happy.png")
  (sgl:display-sprite "cd happy.png" 1 1 0 0 :yshift 200 :xshift 50)
  ;;draw panels
  (sgl-gui:draw-ui-panel (car *dialogue-panel*) :y (caddr *dialogue-panel*))
  (sgl-gui:write-text font-path format (nth (text-format-line-number format) *dialogue-list*)))

(defun dialogue-example ()
  (sdl2-image:init '(:png))
  (glfw:with-init-window (:title "Test Window" :width 800 :height 1000)
    (let ((font-path "noto-vegas/")) 
      (glfw:set-window-size-callback 'update-viewport)
      (glfw:set-key-callback 'quit-on-escape)
      (glfw:set-key-callback 'next-text)
      ;; .3 .3. 3 for gray
      (gl:clear-color 0 0 1 1)
      (loop until (glfw:window-should-close-p)
            do (gl:clear :color-buffer-bit)
            do (background)
            do (draw-scene-1 font-path *tf*)
            do (gl:clear)
	    do (glfw:poll-events)
	    do (glfw:swap-buffers))
      (setf (text-format-line-number *tf*) 0))))
